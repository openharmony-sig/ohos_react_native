# React Native 鸿蒙化版本信息

当前React Native鸿蒙版本基于社区RN 0.72.5进行适配，发布版本信息如下：

| 名称                          | 版本号                            |
| ----------------------------- | -------------------------------|
| react-native-harmony.tgz        | 0.72.61 |
| react-native-harmony-cli.tgz    | 0.0.28 |
| react_native_openharmony-5.1.0.305.har                          | 0.72.61 |
| react_native_openharmony_release-5.1.0.305.har                  | 5.1.0.305 |

配套IDE、SDK版本和手机ROM:

| 名称                          | 版本号                            |
| ----------------------------- | -------------------------------|
| DevEco Studio     | DevEco Studio 5.1.0.258 |
| HarmonyOS SDK     | HarmonyOS SDK 5.1.0.54(SP2) |
| 手机ROM           | ALN-AL00 205.1.0.54(SP30DEVC00E54R4P1) <br> ALN-AL80 205.1.0.54(SP30DEVC00E54R4P1) <br> BRA-AL00 205.1.0.54(SP30DEVC00E54R4P1) |